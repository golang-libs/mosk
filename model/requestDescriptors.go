package model

import (
	"context"
	"gitlab.com/golang-libs/mosk.git/validation"
	"mime/multipart"
)

type ProcessingStep func(RequestData, ResponseData)

type RequestTypeDescriptor struct {
	//URI                  string
	SecCode string
	//HttpMethod           string
	Name                 string
	ChainStart           ProcessingStep
	DefaultAccessGroups  []string
	MethodAccessOverride []string
	Protected            bool
	ServesFile           bool
	// Персональный дедлеттеринг
	DeadLetteringQueue string
}

type LegacyMode struct {
	IsON          bool
	DefaultMethod RequestTypeDescriptor
}

type DescriptorMap map[string]map[string]RequestTypeDescriptor

type RequestTypeDescriptors struct {
	Queue string
	// Общая очередь ошибочных - назначится всем пустым DeadLetteringQueue в группе
	DeadLetteringQueue string
	ReplyNeeded        bool
	ReplyQueue         string
	Descriptors        DescriptorMap // разбитие по http мтодам
	// аккуратнее с указанием этого параметра, он может продублировать ваши сообщения, если в RMQ указана деадлеттер очередь(режим "дедлеттером занят RMQ")
	// И указана DeadLetteringQueue очередь для эндпоинта(режим "дедлеттером занята либа Mosk"). Они оба отправят по сообщению в DeadLetterQueue
	MustNackOnError     bool // при ошибке в обрабочиках сделает nack(или его аналог) для сообщения
	ShouldRequeueOnNack bool
	// Для кафки: режим легаси, для методов, которые раньше не работали через Mosk - отошлет все тело, без роутинга в дефолтный метод
	// Позволит быстро перекидать легаси-вещи, универсализировать их
	LegacyMode LegacyMode
}

type RequestData interface {
	GetClient() MoskClient
	GetUser() MoskUser
	GetParams() map[string]string
	GetAllParams() map[string][]string
	GetHeaders() map[string]string
	GetBody() []byte
	GetMethod() string
	GetURI() string
	SetUser(user MoskUser)
	SetClient(client MoskClient)
	GetLocale() string
	SetLocale(string)
	GetRequestId() string
	GetSource() string
	GetMultipartForm() *multipart.Form
	GetContext() context.Context
	SetContext(ctx context.Context)
}

type MoskRequestData struct {
	User          MoskUser
	Client        MoskClient
	Params        map[string]string
	AllParams     map[string][]string
	Headers       map[string]string
	Body          []byte
	Method        string
	RMQMethod     string
	URI           string
	Locale        string
	RequestId     string
	Source        string
	Ctx           context.Context
	MultipartForm *multipart.Form
}

func (mrd MoskRequestData) GetRequestId() string {
	return mrd.RequestId
}

func (mrd MoskRequestData) GetSource() string {
	return mrd.Source
}

func (mrd MoskRequestData) GetLocale() string {
	return mrd.Locale
}

func (mrd *MoskRequestData) SetLocale(loc string) {
	mrd.Locale = loc
}

func NewRequestData() MoskRequestData {
	return MoskRequestData{User: MoskUser{}, Client: MoskClient{}, Params: make(map[string]string), AllParams: make(map[string][]string),
		Headers: make(map[string]string), Body: []byte{}}
}

func (mrd MoskRequestData) GetMethod() string {
	return mrd.Method
}

func (mrd MoskRequestData) GetURI() string {
	return mrd.URI
}

func (mrd MoskRequestData) GetBody() []byte {
	return mrd.Body
}

func (mrd MoskRequestData) GetUser() MoskUser {
	return mrd.User
}

func (mrd MoskRequestData) GetClient() MoskClient {
	return mrd.Client
}

func (mrd MoskRequestData) GetParams() map[string]string {
	return mrd.Params
}

func (mrd MoskRequestData) GetAllParams() map[string][]string {
	return mrd.AllParams
}

func (mrd MoskRequestData) GetHeaders() map[string]string {
	return mrd.Headers
}

func (mrd MoskRequestData) GetMultipartForm() *multipart.Form {
	return mrd.MultipartForm
}

func (mrd *MoskRequestData) GetContext() context.Context {
	return mrd.Ctx
}

// Sets the user for next processing steps.
func (mrd *MoskRequestData) SetUser(user MoskUser) {
	mrd.User = user
}

func (mrd *MoskRequestData) SetClient(client MoskClient) {
	mrd.Client = client
}

func (mrd *MoskRequestData) SetContext(ctx context.Context) {
	mrd.Ctx = ctx
}

type ResponseData interface {
	GetBody() []byte
	SetBody(b []byte)
	// Для экономии строчек
	SetMarshalResults(b []byte, err error)
	//GetHeaders() map[string]string
	AddHeaders(map[string]string)
	GetError() error
	SetErr(error)
	GetCode() int
	SetCode(int)
	GetMessage() string
	SetMessage(string)
	GetExtCode() int
	SetExtCode(int)
	GetRequestId() string
	SetFileToServe(name string, data []byte)
	GetValidationErrors() []validation.ValidationErrorInterface
	AddValidationError(err validation.ValidationErrorInterface)
	AddValidationErrors(err []validation.ValidationErrorInterface)
	GetContext() context.Context
	SetContext(ctx context.Context)
}

type MoskResponseData struct {
	Headers          map[string]string
	Body             []byte
	Code             int
	ExtCode          int
	Err              error
	RequestId        string
	Message          string
	FileDataToServe  []byte
	FileNameToServe  string
	ValidationErrors []validation.ValidationErrorInterface
	Ctx              context.Context
}

func (mrd *MoskResponseData) GetRequestId() string {
	return mrd.RequestId
}

func NewResponseData() MoskResponseData {
	return MoskResponseData{Headers: make(map[string]string), Body: []byte{}}
}

func (mrd *MoskResponseData) SetMarshalResults(b []byte, err error) {
	mrd.Body = b
	mrd.Err = err
}

func (mrd MoskResponseData) GetBody() []byte {
	return mrd.Body
}

func (mrd *MoskResponseData) SetBody(b []byte) {
	mrd.Body = b
}

func (mrd *MoskResponseData) GetValidationErrors() []validation.ValidationErrorInterface {
	return mrd.ValidationErrors
}

func (mrd *MoskResponseData) AddValidationError(err validation.ValidationErrorInterface) {
	mrd.ValidationErrors = append(mrd.ValidationErrors, err)
}

func (mrd *MoskResponseData) AddValidationErrors(errs []validation.ValidationErrorInterface) {
	mrd.ValidationErrors = append(mrd.ValidationErrors, errs...)
}

//
//func (lrd MoskResponseData) GetHeaders() map[string]string {
//	return lrd.Headers
//}

func (mrd *MoskResponseData) AddHeaders(h map[string]string) {
	for k, v := range h {
		mrd.Headers[k] = v
	}
}

func (mrd MoskResponseData) GetError() error {
	return mrd.Err
}

func (mrd *MoskResponseData) SetErr(e error) {
	mrd.Err = e
}

func (mrd MoskResponseData) GetCode() int {
	return mrd.Code
}

func (mrd *MoskResponseData) SetCode(c int) {
	mrd.Code = c
}

func (mrd MoskResponseData) GetExtCode() int {
	return mrd.ExtCode
}

func (mrd *MoskResponseData) SetExtCode(c int) {
	mrd.ExtCode = c
}

func (mrd MoskResponseData) GetMessage() string {
	return mrd.Message
}

func (mrd *MoskResponseData) SetMessage(m string) {
	mrd.Message = m
}

func (mrd *MoskResponseData) SetFileToServe(name string, data []byte) {
	mrd.FileNameToServe = name
	mrd.FileDataToServe = data
}

func (mrd *MoskResponseData) GetContext() context.Context {
	return mrd.Ctx
}

func (mrd *MoskResponseData) SetContext(ctx context.Context) {
	mrd.Ctx = ctx
}
