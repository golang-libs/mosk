package model

type MethodRMQRequest struct {
	Method string      `json:"method"`
	Data   interface{} `json:"data"`
}
