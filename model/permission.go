package model

import (
	"strings"
	"time"
)

type Permission struct {
	Area       string    `json:"area"`
	Topic      string    `json:"topic"`
	Operation  string    `json:"operation"`
	Expiration time.Time `json:"-"`
	OwnerID    bool      `json:"-"`
}

type Permissions []Permission

func (prms Permissions) IsPermitted(service, group, name string) bool {
	for _, p := range prms {
		if (p.Operation == "*" || strings.EqualFold(p.Operation, name)) &&
			(p.Topic == "*" || strings.EqualFold(p.Topic, group)) && (strings.EqualFold(p.Area, service)) {
			return true
		}
	}
	return false
}
