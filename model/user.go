package model

type MoskUser struct {
	Usernames    []string
	Active       bool
	Permissions  Permissions // включая ролевые
	Roles        Roles
	ID           string
	OriginalData interface{} // Оригинальные данные для конечного пользователя(что бы не пихатькучу полей)
}
