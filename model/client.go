package model

type MoskClient struct {
	Name         string
	Active       bool
	Permissions  Permissions
	ID           int64
	OriginalData interface{} // Оригинальные данные для конечного пользователя(что бы не пихатькучу полей)
}
