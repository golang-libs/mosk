package requestHandling

import (
	"bytes"
	"context"
	"crypto/rand"
	"fmt"
	"github.com/hashicorp/go-uuid"
	"gitlab.com/golang-libs/mosk.git/common"
	"gitlab.com/golang-libs/mosk.git/model"
	"go.uber.org/zap"
	"io"
	"log"
	"net/http"
	"time"
)

type RequestHandlerFunc func(w http.ResponseWriter, r *http.Request, descriptor model.RequestTypeDescriptor) error

// СТРОГО для логирования - упрощения чтения логов, НЕ использовать для чего-то серьезного
func randID() string {
	b := make([]byte, 7)
	_, err := rand.Read(b)
	if err != nil {
		return ""
	}
	return fmt.Sprintf("%x-%x", b[0:3], b[4:])
}

func collectHeadersToMap(h http.Header) map[string]string {
	headers := make(map[string]string)
	for name, value := range h {
		if len(value) > 0 {
			headers[name] = value[0]
		}
	}
	return headers
}

func RegisterEndpoints(descriptors ...model.RequestTypeDescriptors) error {
	for _, descriptorsPart := range descriptors {
		for uri, methods := range descriptorsPart.Descriptors {
			// todo - remove when 1.22
			if len(methods) > 0 {
				return common.ErrMultipleMethodsByURI
			}
			// todo - remove when 1.22
			for _, descriptor := range methods {
				http.HandleFunc(uri, httpChain(descriptor.ChainStart))
			}
		}
	}
	return nil
}

// На случай если что то капитально отвалится
func formHttpError(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	log.Println(err)
	return
}

func httpChain(firstStep model.ProcessingStep) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		i := 0
		var err error
		id := r.Header.Get("fcbrequestid")
		if id == "" {
			id, _ = uuid.GenerateUUID()
		}
		responseData := model.NewResponseData()
		responseData.Code = http.StatusOK
		responseData.RequestId = id
		responseData.Ctx = context.WithoutCancel(context.Background())

		requestData := model.NewRequestData()
		requestData.Method = r.Method
		requestData.URI = r.URL.Path
		requestData.Headers = collectHeadersToMap(r.Header)
		requestData.RequestId = id
		requestData.Ctx = r.Context()
		err = r.ParseMultipartForm(15 << 20)
		requestData.MultipartForm = r.MultipartForm
		xRealIP := r.Header.Get("X-Real-IP")
		if xRealIP == "" {
			requestData.Source = r.Header.Get("X-Forwarded-For")
		}

		i++
		zap.S().Debug("Mosk request http", i, time.Since(start))

		values := r.URL.Query()
		for idx, value := range values {
			if len(value) > 0 {
				requestData.Params[idx] = value[0]
				for _, val := range value {
					if requestData.AllParams[idx] == nil {
						requestData.AllParams[idx] = make([]string, 0)
					}
					requestData.AllParams[idx] = append(requestData.AllParams[idx], val)
				}

			}
		}

		requestData.Body, err = io.ReadAll(r.Body)
		if err != nil {
			formHttpError(w, err)
			return
		}
		i++
		zap.S().Debug("Mosk request http", i, time.Since(start))

		firstStep(&requestData, &responseData)
		i++
		zap.S().Debug("Mosk request http", i, time.Since(start))

		for hKey, hValue := range responseData.Headers {
			w.Header().Set(hKey, hValue)
		}
		i++
		zap.S().Debug("Mosk request http", i, time.Since(start))

		// Сервим файл
		if responseData.FileDataToServe != nil && responseData.FileNameToServe != "" {
			w.Header().Set("Content-Length", fmt.Sprint(len(responseData.FileDataToServe)))
			w.Header().Set("Content-Disposition", "attachment; filename="+responseData.FileNameToServe)
			//w.WriteHeader(responseData.Code) // Да, именно дублирование, т.к оно завершает последовательность формирования хедеров
			http.ServeContent(w, r, "report.docx", time.Now(), bytes.NewReader(responseData.FileDataToServe))
			return
		}

		w.WriteHeader(responseData.Code)

		_, err = w.Write(responseData.Body)
		if err != nil {
			formHttpError(w, err)
			return
		}
		i++
		zap.S().Debug("Mosk request http", i, time.Since(start))
	}
}
