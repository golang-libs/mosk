package router

import (
	"gitlab.com/golang-libs/mosk.git/model"
)

var router = make(map[string]map[string]model.RequestTypeDescriptor)

func FindMethodByRoute(method, httpMethod string) (val model.RequestTypeDescriptor, ok bool) {
	valMap, ok := router[method]
	if !ok {
		return model.RequestTypeDescriptor{}, false
	}

	if len(valMap) == 1 {
		for _, val = range valMap {
			return val, true
		}
	}

	if len(valMap) > 1 && httpMethod == "" {
		return model.RequestTypeDescriptor{}, false
	}

	val, ok = valMap[httpMethod]

	return
}

func AddRoute(method string, httpMethod string, descriptor model.RequestTypeDescriptor, descriptors model.RequestTypeDescriptors) {
	if descriptor.DeadLetteringQueue == "" {
		descriptor.DeadLetteringQueue = descriptors.DeadLetteringQueue
	}

	if router[method] == nil {
		router[method] = make(map[string]model.RequestTypeDescriptor)
	}

	router[method][httpMethod] = descriptor
}
