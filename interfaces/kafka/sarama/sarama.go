package sarama

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/IBM/sarama"
	"github.com/pkg/errors"
	"gitlab.com/golang-libs/mosk.git/common"
	kafkacommons "gitlab.com/golang-libs/mosk.git/interfaces/kafka"
	"gitlab.com/golang-libs/mosk.git/interfaces/router"
	"gitlab.com/golang-libs/mosk.git/model"
	"go.uber.org/zap"
	"net/http"
	"strconv"
)

func collectHeadersToMap(h []*sarama.RecordHeader) map[string]string {
	headers := make(map[string]string)
	for _, header := range h {
		headers[string(header.Key)] = string(header.Value)
	}
	return headers
}

var DefaultHeaders = map[string]interface{}{"Content-Type": "application/json"}

func processMessage(message *sarama.ConsumerMessage, replyProducer sarama.SyncProducer, replyTopic string,
	legacyMode bool, defaultMethod model.RequestTypeDescriptor) (err error) {

	id := message.Topic + strconv.Itoa(int(message.Partition)) + strconv.FormatInt(message.Offset, 10)
	responseData := model.NewResponseData()
	responseData.RequestId = id
	responseData.Ctx = context.WithoutCancel(context.Background())

	requestData := model.NewRequestData()
	requestData.RequestId = id
	requestData.Source = id

	// initialize defaults
	requestData.Method = defaultMethod.HttpMethod
	requestData.URI = defaultMethod.URI
	requestData.Body = message.Value
	val := defaultMethod

	// нам могут передать исправленный вариант, даже при включенном LegacyMode
	data, err := common.FindRoute(message.Value)
	if err != nil {
		return errors.Wrap(err, "WRONG_JSON_FORMAT")
	}

	if !legacyMode {
		if data.GetMethod() == "" {
			return errors.Errorf("METHOD_NOT_FOUND")
		}

		var ok bool
		// Пробить хендлер по роутеру
		val, ok = router.FindMethodByRoute(data.GetMethod())
		if !ok {
			return errors.Errorf("Метод не найден")
		}

		requestData.Method = val.HttpMethod
		requestData.URI = val.URI
		requestData.Body = data.Data
	}

	requestData.Ctx = context.Background()
	// Положим в ГЕТ параметры
	if val.HttpMethod == http.MethodGet {
		err := json.Unmarshal(requestData.Body, &requestData.Params)
		if err != nil {
			return errors.Errorf("Неверный формат сообщения: GET принимает только строки")
		}
	}

	// Преобразовать данные в зависимости от типа запроса
	requestData.Headers = collectHeadersToMap(message.Headers)
	// Вызвать чейн
	// Получить массив байт для ответа
	val.ChainStart(&requestData, &responseData)

	if replyProducer != nil && (responseData.GetError() == nil || !errors.Is(responseData.GetError(), common.ErrorNotMyMessage)) {
		if replyTopic == "" {
			replyTopic = requestData.Headers["replyTo"]
		}

		var headers = []sarama.RecordHeader{{Key: []byte("code"), Value: []byte(strconv.Itoa(responseData.Code))}}
		for key, val := range responseData.Headers {
			headers = append(headers, sarama.RecordHeader{
				Key:   []byte(key),
				Value: []byte(val),
			})
		}

		msg := &sarama.ProducerMessage{
			Topic: replyTopic,
			//Partition: -1,
			Value:   sarama.StringEncoder(responseData.GetBody()),
			Headers: headers,
		}

		_, _, err := replyProducer.SendMessage(msg)
		if err != nil {
			zap.S().Errorw(fmt.Sprintf("Error replying to message %s, error is %v", id, err),
				"request", id,
			)
		}
	}

	return nil
}

func logKafkaError(err error) {
	zap.S().Error(err)
}

func InitKafkaListener(config kafkacommons.KafkaConfig, allEndpointsOnConn ...model.RequestTypeDescriptors) error {
	kafkaCfg := sarama.NewConfig()
	kafkaCfg.Consumer.Return.Errors = true
	// CHECK!
	//kafkaCfg.Consumer.Offsets.AutoCommit.Enable = false
	kafkaCfg.Consumer.Offsets.Initial = sarama.OffsetOldest

	kafkaCfg.Producer.RequiredAcks = sarama.WaitForAll
	kafkaCfg.Producer.Retry.Max = 5
	kafkaCfg.Producer.Return.Successes = true

	kafkaUrls := config.Brokers

	// Create new consumer
	master, err := sarama.NewConsumer(kafkaUrls, kafkaCfg)
	if err != nil {
		panic(err)
	}
	// Инициализируем каналы
	for _, endpointsBunch := range allEndpointsOnConn {

		if endpointsBunch.LegacyMode.IsON {
			if len(endpointsBunch.Descriptors) != 1 {
				return errors.New("legacy mode can have only one endpoint")
			}
			endpointsBunch.LegacyMode.DefaultMethod = endpointsBunch.Descriptors[0]
		}

		for _, endpoint := range endpointsBunch.Descriptors {
			router.AddRoute(endpoint.URI, endpoint, endpointsBunch)
		}

		reqQueue := endpointsBunch.Queue
		replyQueue := endpointsBunch.ReplyQueue

		partitions, err := master.Partitions(reqQueue)
		if err != nil {
			return err
		}
		consumer, err := master.ConsumePartition(endpointsBunch.Queue, partitions[0], sarama.OffsetOldest)
		if err != nil {
			return err
		}
		// пока инициализирую по продьюсеру на консьюмера
		// возможно, имеет смысл в одном продьюсере на все приложение? Is it "kafka way"?
		var replyProducer sarama.SyncProducer
		if endpointsBunch.ReplyNeeded {
			replyProducer, err = sarama.NewSyncProducer(kafkaUrls, kafkaCfg)
			if err != nil {
				return err
			}
		}

		go func(processedEndpoints model.RequestTypeDescriptors, consumer sarama.PartitionConsumer) {
			for {
				select {
				case consumerError := <-consumer.Errors():
					logKafkaError(consumerError)

				case msg := <-consumer.Messages():
					err = processMessage(msg, replyProducer, replyQueue, processedEndpoints.LegacyMode.IsON,
						processedEndpoints.LegacyMode.DefaultMethod)
					if err != nil {
						logKafkaError(err)
					}
				}
			}
		}(endpointsBunch, consumer)
	}
	return nil
}
