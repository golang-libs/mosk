package rabbitMQ

import (
	"context"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"github.com/pkg/errors"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/golang-libs/mosk.git"
	"gitlab.com/golang-libs/mosk.git/common"
	"gitlab.com/golang-libs/mosk.git/interfaces/router"
	"gitlab.com/golang-libs/mosk.git/model"
	"log"
	"time"
)

// RMQConnection Данная "конструхсыя" представляет собой обертку рэббитового коннекта
// + все каналы уничтожения каналов этого коннекта. При обрыве соединения оно оповестит об этом в connCloseChan
// после этого инициатор соединения разошлет слушающим каналам уведомления, что пора совершить сэпукку
// Затем попытается реинициализировать соединение и слушающие каналы для восстановления нормальной работы сервиса
type RMQConnection struct {
	Conn          *amqp.Connection
	ConnCloseChan chan *amqp.Error
	Terminators   []chan bool
}

type MethodInterface interface {
	GetHttpMethod() string
	GetMethod() string
	GetData() (string, error)
}

func CollectHeadersToMap(h map[string]interface{}) map[string]string {
	headers := make(map[string]string)
	for name, header := range h {
		if header != nil {
			switch header.(type) {
			case string:
				headers[name] = header.(string)
			}
		}
	}
	return headers
}

func processDelivery(ch *amqp.Channel, d amqp.Delivery, RMQMustNackOnError, shouldRequeueOnNack bool) error {
	var err error
	var mi MethodInterface

	switch d.ContentType {
	case "text/xml":
		method := new(common.XMLMethodStruct)
		err = xml.Unmarshal(d.Body, &method)
		mi = method
	default:
		method := new(common.MethodStruct)
		err = json.Unmarshal(d.Body, &method)
		mi = method
	}

	if mi.GetMethod() == "" {
		return errors.Errorf("Пустой метод")
	}

	responseData := model.NewResponseData()
	responseData.RequestId = d.MessageId
	responseData.Ctx = context.WithoutCancel(context.Background())

	requestData := model.NewRequestData()
	requestData.RequestId = d.MessageId
	requestData.Source = d.UserId

	// Пробить хендлер по роутеру
	val, ok := router.FindMethodByRoute(mi.GetMethod(), mi.GetHttpMethod())
	if !ok {
		return errors.Errorf("Метод не найден")
	}

	requestData.Method = mi.GetHttpMethod()
	requestData.URI = mi.GetMethod()

	data, err := mi.GetData()
	if err != nil {
		return err
	}

	requestData.Body = []byte(data)
	requestData.Ctx = context.Background()
	requestData.Params, err = common.GetMapByReqType(mi.GetHttpMethod(), requestData.Body)
	if err != nil {
		return errors.Errorf("Неверный формат сообщения")
	}

	// Преобразовать данные в зависимости от типа запроса
	requestData.Headers = CollectHeadersToMap(d.Headers)
	// Вызвать чейн
	// Получить массив байт для ответа
	val.ChainStart(&requestData, &responseData)

	respHeaders := map[string]interface{}{}
	respHeaders["code"] = responseData.Code
	respHeaders["message"] = responseData.Message
	for key, val := range responseData.Headers {
		respHeaders[key] = val
	}

	if responseData.Err != nil {
		if RMQMustNackOnError {
			errNack := d.Nack(false, shouldRequeueOnNack)
			if errNack != nil {
				log.Printf("Error on nack %v", errNack)
			}
		}

		if val.DeadLetteringQueue != "" {
			return ch.PublishWithContext(context.Background(),
				"",                     // exchange
				val.DeadLetteringQueue, // routing key
				false,                  // mandatory
				false,                  // immediate
				amqp.Publishing{
					ContentType:   "application/json",
					CorrelationId: d.CorrelationId,
					Headers:       d.Headers,
					Body:          d.Body,
				})
		}
	}

	if d.ReplyTo != "" && d.CorrelationId != "" {
		// Отправить обратно, по corellationID
		return ch.PublishWithContext(context.Background(),
			"",        // exchange
			d.ReplyTo, // routing key
			false,     // mandatory
			false,     // immediate
			amqp.Publishing{
				ContentType:   "application/json",
				CorrelationId: d.CorrelationId,
				Headers:       respHeaders,
				Body:          responseData.Body,
			})
	}

	return nil
}

func InitRMQConnection(rabbit RabbitConfig) (newConn *amqp.Connection, connCloseChan chan *amqp.Error, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = mosk.PanicToError(r)
		}
	}()

	newConn, err = amqp.Dial(fmt.Sprintf("%s://%s:%s@%s/%s", rabbit.Protocol, rabbit.User, rabbit.Password, rabbit.Url, rabbit.VirtualHost))
	if err != nil {
		return nil, nil, errors.Wrap(err, "")
	}
	connCloseChan = newConn.NotifyClose(make(chan *amqp.Error))

	return newConn, connCloseChan, nil
}

func PrepareListenerChannel(connection *amqp.Connection, endpoints model.RequestTypeDescriptors) (chan bool, error) {
	for uri, methods := range endpoints.Descriptors {
		for method, endpoint := range methods {
			router.AddRoute(uri, method, endpoint, endpoints)
		}
	}
	newTerminator := make(chan bool)
	ch, err := connection.Channel()
	if err != nil {
		return newTerminator, errors.Wrap(err, "Невозможно открыть канал")
	}

	q, err := ch.QueueDeclare(endpoints.Queue, true, false, false, false, nil)
	if err != nil {
		return newTerminator, errors.Wrap(err, "Невозможно объявить очередь")
	}

	err = ch.Qos(1, 0, false)
	if err != nil {
		return newTerminator, errors.Wrap(err, "Невозможно объявить QoS")
	}

	msgs, err := ch.Consume(q.Name, "", false, false, false, false, nil)
	if err != nil {
		return newTerminator, errors.Wrap(err, "Невозможно объявить консьюмера")
	}

	go func() {
		for {
			select {
			case d := <-msgs:
				err := processDelivery(ch, d, endpoints.MustNackOnError, endpoints.ShouldRequeueOnNack)
				if err != nil {
					formRMQErr(ch, d, err)
				}
				_ = d.Ack(false)
			case <-newTerminator:
				_ = ch.Close()
				return
			default:
				time.Sleep(10 * time.Millisecond)
			}
		}
	}()

	return newTerminator, err
}

func formRMQErr(ch *amqp.Channel, d amqp.Delivery, err error) {
	if d.ReplyTo != "" && d.CorrelationId != "" {
		log.Println(err) // Последний бастион - логируем в файл
		b, err := json.Marshal(model.CustomResponse{Result: -1, ResultDescr: "Неизвестная ошибка"})
		if err != nil {
			log.Println(err)
		}
		// Отправить обратно, по correlation ID
		_ = ch.PublishWithContext(context.Background(),
			"",        // exchange
			d.ReplyTo, // routing key
			false,     // mandatory
			false,     // immediate
			amqp.Publishing{
				ContentType:   "application/json",
				CorrelationId: d.CorrelationId,
				Body:          b,
			})
	}
}

func declareAnswerQueue(ch *amqp.Channel) (msgs <-chan amqp.Delivery, qName, corrId string, err error) {
	corrId = common.RandomString(64)
	q, err := ch.QueueDeclare("", false, true, true, false, nil)
	if err != nil {
		return
	}

	qName = q.Name

	msgs, err = ch.Consume(q.Name, "", true, false, false, false, nil)
	return
}

func marshalByType(cType string, o interface{}) ([]byte, error) {
	switch cType {
	case "application/xml", "text/xml":
		return xml.Marshal(o)
	default:
		return json.Marshal(o)
	}
}

func SendNotificationRMQRequestToConn(conn *amqp.Connection, data interface{}, header map[string]interface{},
	method, queue, correlationId, contentType string) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = mosk.PanicToError(r)
			log.Printf("Error passing `%v`, err: %v", data, err)
		}
	}()

	ch, err := conn.Channel()
	if err != nil {
		return errors.Wrap(err, "")
	}
	defer ch.Close()

	rmqReq := data
	if method != "" {
		rmqReq = model.MethodRMQRequest{Data: data, Method: method}
	}

	b, err := marshalByType(contentType, rmqReq)
	if err != nil {
		return errors.Wrap(err, "")
	}

	if mosk.DebugMode {
		log.Println("Publishing message to", queue, ":", string(b))
	}

	return ch.PublishWithContext(context.Background(),
		"",    // exchange
		queue, // routing key
		false, // mandatory
		false, // immediate,
		amqp.Publishing{
			ContentType:   contentType,
			CorrelationId: correlationId,
			ReplyTo:       "",
			Body:          b,
			Headers:       header,
		})
}

func SendAnswerableRMQRequestToConn(conn *amqp.Connection, data interface{}, requestHeaders map[string]interface{}, method,
	queue string, waitSec time.Duration) (response []byte, responseHeaders map[string]interface{}, err error) { // Может возвращать все хедеры?
	response = []byte{}
	ch, err := conn.Channel()
	if err != nil {
		return
	}
	defer ch.Close()

	qName, corrId := "", ""
	var msgs <-chan amqp.Delivery

	msgs, qName, corrId, err = declareAnswerQueue(ch)
	if err != nil {
		return
	}

	rmqReq := data
	if method != "" {
		rmqReq = model.MethodRMQRequest{Data: data, Method: method}
	}

	bytes, err := json.Marshal(rmqReq)
	if err != nil {
		return
	}

	err = ch.PublishWithContext(context.Background(),
		"",    // exchange
		queue, // routing key
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			AppId:         mosk.AppId,
			ContentType:   "application/json",
			CorrelationId: corrId,
			ReplyTo:       qName,
			Body:          bytes,
			Headers:       requestHeaders,
		})
	if err != nil {
		return
	}
	if mosk.DebugMode {
		log.Printf("Published message to %v: %v\n", queue, string(bytes))
	}

	// Не горутина, т.к ответ будет только один и затем вызов умирает
	select {
	case d := <-msgs:
		if corrId == d.CorrelationId {
			responseHeaders = d.Headers
			response = d.Body
		}
	case <-time.After(waitSec):
		return response, map[string]interface{}{}, errors.Errorf("Нет ответа от RMQ")
	}

	return
}

func GetRecoverableRMQConnection(rabbit RabbitConfig) (conn *amqp.Connection, err error) {
	var connCloseChan chan *amqp.Error
	conn, connCloseChan, err = InitRMQConnection(rabbit)
	if err != nil {
		return conn, errors.Wrap(err, "")
	}

	go func() {
		for {
			select {
			case rmqErr := <-connCloseChan:
				log.Printf("Active connection to %s closed, reason: %v", rabbit.Url, rmqErr)
				newConn, newConnCloseChan, err := InitRMQConnection(rabbit)
				if err != nil {
					time.Sleep(time.Second)
					continue
				}
				conn = newConn
				connCloseChan = newConnCloseChan
			default:
				time.Sleep(time.Millisecond * 10)
				continue
			}
		}
	}()

	return conn, nil
}

func InitUnrecoverableRabbitMQListener(rabbit RabbitConfig, allEndpointsOnConn ...model.RequestTypeDescriptors) error {
	newConn, _, err := InitRMQConnection(rabbit)
	if err != nil {
		return err
	}

	// Инициализируем каналы
	for _, endpointsBunch := range allEndpointsOnConn {
		if endpointsBunch.Queue == "" {
			continue
		}

		_, err := PrepareListenerChannel(newConn, endpointsBunch)
		if err != nil {
			return err
		}
	}
	return nil
}

func InitRabbitMQListener(rabbit RabbitConfig, allEndpointsOnConn ...model.RequestTypeDescriptors) error {
	newConn, connCloseChan, err := InitRMQConnection(rabbit)
	if err != nil {
		return err
	}

	res := RMQConnection{Conn: newConn, Terminators: make([]chan bool, 0), ConnCloseChan: connCloseChan}

	// Инициализируем каналы
	for _, endpointsBunch := range allEndpointsOnConn {
		if endpointsBunch.Queue == "" {
			continue
		}
		newTerminator, err := PrepareListenerChannel(res.Conn, endpointsBunch)
		if err != nil {
			// Либо все каналы, либо ничего
			for _, terminator := range res.Terminators {
				terminator <- true
			}
			return err
		}
		res.Terminators = append(res.Terminators, newTerminator)
	}

	go func() {
		for {
			reason, ok := <-res.ConnCloseChan
			if ok {

				log.Printf("Listener connection to %s closed, reason: %v", rabbit.Url, reason)

				// Прикрываем открытые каналы
				for _, terminator := range res.Terminators {
					terminator <- true
				}

				for {
					err = InitRabbitMQListener(rabbit, allEndpointsOnConn...)
					if err != nil {
						time.Sleep(time.Second)
						continue
					}
					break
				}
			} else {
				time.Sleep(500 * time.Millisecond)
			}
		}
	}()

	return nil
}
