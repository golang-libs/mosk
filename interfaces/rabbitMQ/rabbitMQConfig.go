package rabbitMQ

type RabbitConfig struct {
	Protocol      string `json:"protocol"`
	User          string `json:"user"`
	Password      string `json:"password"`
	Url           string `json:"url"`
	VirtualHost   string `json:"virtualHost"`
	ReconnectTime int    `json:"reconnectTime"`
}
