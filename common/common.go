package common

import (
	"encoding/json"
	"errors"
	"gopkg.in/guregu/null.v3"
	"math/rand"
	"net/http"
	"regexp"
	"strconv"
	"time"
)

// ErrorNotMyMessage ошибка для кафки - если несколько консьюмеров сидят на одной очереди, и код сообщения где то внутри тела
// то можно выкинуть эту ошибку и MOSK поймет, что это сообщение было просто пропущено. Т.е его не надо корммитить, но не надо и
// запускать deadlettering и прочие механизхмы обработки ошибок(выглядит как костыль, но потом по идее перепишем XDATA на новый роутинг)
var ErrorNotMyMessage = errors.New("not my message")
var ErrMultipleMethodsByURI = errors.New("multiple methods by uri")

func SqlFilter(data interface{}) (res string) {
	re := regexp.MustCompile("['!#$&()*/;<=>?^`|~]")
	switch data.(type) {
	case string:
		res = re.ReplaceAllString(data.(string), "")
	case json.RawMessage:
		res = re.ReplaceAllString(string(data.(json.RawMessage)), "")
	case null.String:
		res = re.ReplaceAllString(data.(null.String).String, "")
	}
	return res
}

func IsOKCode(code int) bool {
	// Либо задан НЕ ОК код, либо код вообще не задан
	return (code < http.StatusMultipleChoices && code >= http.StatusOK) || code == 0
}

func GetIntParamOrDefault(param string, def int) int {
	if param == "" {
		return def
	}

	temp, err := strconv.Atoi(param)
	if err != nil {
		return def
	}

	return temp
}

func GetInt64ParamOrDefault(param string, def int64) int64 {
	if param == "" {
		return def
	}

	temp, err := strconv.ParseInt(param, 20, 64)
	if err != nil {
		return def
	}

	return temp
}

func GetIntParam(param string) *int {
	if param == "" {
		return nil
	}

	temp, err := strconv.Atoi(param)
	if err != nil {
		return nil
	}

	return &temp
}

func GetBoolParam(param string) *bool {
	if param == "" {
		return nil
	}

	temp, err := strconv.ParseBool(param)
	if err != nil {
		return nil
	}

	return &temp
}

func GetInt64Param(param string) *int64 {
	if param == "" {
		return nil
	}

	temp, err := strconv.ParseInt(param, 10, 64)
	if err != nil {
		return nil
	}

	return &temp
}

func GetStringParam(param string) *string {
	if param == "" {
		return nil
	}

	re, err := regexp.Compile(`['=+;:]`)
	if err != nil {
		return nil
	}

	param = re.ReplaceAllString(param, "")

	return &param
}

func GetTimeParam(param string) *time.Time {
	t, err := time.Parse("2006-01-02T15:04:05Z", param)
	if err != nil {
		return nil
	}

	return &t
}

func GetDateParam(param string) *time.Time {
	t, err := time.Parse("2006-01-02", param)
	if err != nil {
		return nil
	}

	return &t
}

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func RandomString(l int) string {
	bytes := make([]byte, l)
	rand.Seed(time.Now().UnixNano() - 100500)
	for i := 0; i < l; i++ {
		bytes[i] = byte(randInt(65, 90))
	}
	return string(bytes)
}

func GetMapByReqType(method string, b []byte) (m map[string]string, err error) {
	if method == http.MethodGet {
		m = map[string]string{}
		err = json.Unmarshal(b, &m)
		return
	}
	return
}

type XMLMethodStruct struct {
	HttpMethod string    `xml:"HttpMethod"`
	Method     string    `xml:"Method"`
	Data       DataValue `xml:"Data"` // todo
}

type DataValue struct {
	Value string `xml:",innerxml"`
}

func (m *XMLMethodStruct) GetMethod() string {
	return m.Method
}

func (m *XMLMethodStruct) GetHttpMethod() string {
	return m.HttpMethod
}

func (m *XMLMethodStruct) GetData() (string, error) {
	if m.Data.Value == "" {
		return "", errors.New("receive empty data")
	}

	return m.Data.Value, nil
}

type MethodStruct struct {
	HttpMethod string          `json:"httpMethod"`
	Method     string          `json:"method"`
	Data       json.RawMessage `json:"data"`
}

func (m MethodStruct) GetMethod() string {
	return m.Method
}

func (m MethodStruct) GetHttpMethod() string {
	return m.HttpMethod
}

func (m MethodStruct) GetData() (string, error) {
	if len(m.Data) == 0 {
		return "", errors.New("receive empty data")
	}

	return string(m.Data), nil
}

func FindRoute(body []byte) (method MethodStruct, err error) {
	// initialize routing
	data := MethodStruct{} // todo xml
	err = json.Unmarshal(body, &data)
	if err != nil {
		return data, err
	}

	return data, nil
}
